<?php


use Phalcon\Mvc\Application;

error_reporting(E_ALL);
ini_set("display_startup_errors","1");
ini_set("display_errors","1");

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
define('HTTP_SERVER', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['SCRIPT_NAME']), '/.\\') . '/');
define('APP_PATH', realpath('..'));
define('HTTP_IMAGE_DIR', "images/");
define('APP_IMAGE_DIR', APP_PATH . "/public/" . HTTP_IMAGE_DIR);

/**
 * Read the configuration
 */
$config = include APP_PATH . "/apps/config/config.php";

//if (in_array(APPLICATION_ENV, array('development'))) {
    $debug = new \Phalcon\Debug();
    $debug->listen();
//}

require_once APP_PATH . "/apps/library/utf8.php";

/**
 * Include services
 */
require APP_PATH . "/apps/config/services.php";

/**
 * Include loaders
 */
require APP_PATH . "/apps/config/loader.php";

/**
 * Handle the request
 */
$application = new Application();

/**
 * Assign the DI
 */
$application->setDI($di);

/**
 * Include modules
 */
require APP_PATH . "/apps/config/modules.php";

echo $application->handle()->getContent();
