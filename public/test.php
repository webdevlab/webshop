<?php

//This class maps the "robots" table
class Categories extends Phalcon\Mvc\Model
{
    public function initialize()
    {
        $this->setSource("category");
    }
}

//Create a service container with most services
//pre-registered
$di = new Phalcon\DI\FactoryDefault();

//Define the default database connection
$di->set('db', function () {
    return new Phalcon\Db\Adapter\Pdo\Mysql(array(
        'host'     => 'localhost',
        'username' => 'webshop',
        'password' => '532453',
        'dbname'   => 'webshop',
        'charset'  => 'utf8'
    ));
});

$result = Categories::find();
foreach ($result as $category) {
    echo $category->title . '<br>';
}