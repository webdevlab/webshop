DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `image` VARCHAR(255) NULL DEFAULT NULL,
  `parent_id` INT(11) NOT NULL DEFAULT '0',
  `sort_order` INT(3) NOT NULL DEFAULT '0',
  `status` TINYINT(1) NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `modified_at` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `parent_id` (`parent_id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

DROP TABLE IF EXISTS `category_description`;
CREATE TABLE `category_description` (
  `category_id` INT(11) NOT NULL,
  `lang_id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `meta_title` VARCHAR(255) NULL DEFAULT NULL,
  `meta_description` VARCHAR(255) NULL DEFAULT NULL,
  `meta_keyword` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`, `lang_id`),
  INDEX `name` (`name`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(64) NOT NULL,
  `value` TEXT NOT NULL,
  `type` VARCHAR(32) NOT NULL,
  `serialized` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

INSERT INTO `setting` (`id`, `key`, `value`, `type`, `serialized`) VALUES (1, 'default_lang', 'ru', 'system', 0);

DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `alias` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

INSERT INTO `user_group` (`id`, `name`, `alias`) VALUES (1, 'Administrator', 'admin');
INSERT INTO `user_group` (`id`, `name`, `alias`) VALUES (2, 'Manager', 'manager');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` CHAR(60) NOT NULL,
  `groupId` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `groupId` (`groupId`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`lang_id`),
  KEY `name` (`name`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

INSERT INTO `language` (`lang_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
  (1, 'Русский', 'ru', 'ru_RU.UTF-8,ru_RU,russian', 'ru.png', 'russian', 1, 1),
  (2, 'English', 'en', 'en_US.UTF-8,en_US,english', 'gb.png', 'english', 2, 1);

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(13) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `available_at` int(10) unsigned NOT NULL,
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` int(10) unsigned NOT NULL,
  `modified_at` int(10) unsigned DEFAULT NULL,
  `viewed` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

DROP TABLE IF EXISTS `product_description`;
CREATE TABLE `product_description` (
  `product_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NULL DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `tag` text NOT NULL,
  PRIMARY KEY (`product_id`,`lang_id`),
  KEY `name` (`name`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

DROP TABLE IF EXISTS `product_to_category`;
CREATE TABLE `product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `category_id` (`category_id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;
