<?php

$router = new Phalcon\Mvc\Router(false);

$router->setDefaultModule("frontend");

// 404 page
$router->notFound(array(
    'controller'    => 'error',
    'action'        => 'show404'
));

$router->add("/", array(
    'controller' => 'home',
    'action'     => 'index',
))->setName('site');

$router->add("/categories", array(
    'controller' => 'categories',
    'action'     => 'index',
));

$router->add("/categories/:int", array(
    'controller' => 'categories',
    'action'     => 'show',
    'id'        => 1,
));

$router->add("/users", array(
    'controller' => 'users',
    'action'     => 'index',
));

// Backend routers
$router->add("/admin", array(
    'module'     => 'backend',
    'controller' => 'index',
    'action'     => 'index',
));

$router->add("/admin/logout", array(
    'module'     => 'backend',
    'controller' => 'index',
    'action'     => 'logout',
));

$router->add("/admin/categories", array(
    'module'     => 'backend',
    'controller' => 'categories',
    'action'     => 'index',
));

$router->add("/admin/categories/add", array(
    'module'     => 'backend',
    'controller' => 'categories',
    'action'     => 'add',
));

$router->add("/admin/categories/:int", array(
    'module'     => 'backend',
    'controller' => 'categories',
    'action'     => 'edit',
    'cid'        => 1,
));

$router->add("/admin/products", array(
    'module'     => 'backend',
    'controller' => 'products',
    'action'     => 'index',
));

$router->add("/admin/products/add", array(
    'module'     => 'backend',
    'controller' => 'products',
    'action'     => 'add',
));

$router->add("/admin/products/:int", array(
    'module'     => 'backend',
    'controller' => 'products',
    'action'     => 'edit',
    'pid'        => 1,
));

$router->add("/admin/users", array(
    'module'     => 'backend',
    'controller' => 'users',
    'action'     => 'index',
));

$router->add("/admin/users/add", array(
    'module'     => 'backend',
    'controller' => 'users',
    'action'     => 'add',
));

$router->add("/admin/users/:int", array(
    'module'     => 'backend',
    'controller' => 'users',
    'action'     => 'byId',
    'uid'        => 1,
));

$router->add("/admin/settings", array(
    'module'     => 'backend',
    'controller' => 'settings',
    'action'     => 'index',
));

$router->add("/admin/languages", array(
    'module'     => 'backend',
    'controller' => 'languages',
    'action'     => 'index',
));

// Конечные косые черты будут автоматически удалены
$router->removeExtraSlashes(true);

return $router;