<?php

$env = array(
    'production'  => array(
        'database'    => array(
            'host'     => '127.0.0.1',
            'username' => 'webshop',
            'password' => '532453',
            'dbname'   => 'webshop',
            'charset'  => 'utf8'
        )
    ),
    'development' => array(
        'database'    => array(
            'host'     => 'localhost',
            'username' => 'dangelzm',
            'password' => '532453',
            'dbname'   => 'webshop',
            'charset'  => 'utf8'
        )
    ),
);

$app_config = array(
    'database' => $env[APPLICATION_ENV]['database'],
    'security' => array(
        'workFactor' => 7,
        'dsk' => 532453
    ),
    'lang' => 1
);

return new \Phalcon\Config($app_config);