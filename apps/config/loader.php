<?php

use Phalcon\Loader;

$loader = new Loader();

$loader->registerNamespaces(array(
    'Webshop\Frontend\Controllers' => __DIR__ . '/../modules/frontend/controllers/',
    'Webshop\Backend\Controllers'  => __DIR__ . '/../modules/backend/controllers/',
    'Webshop\Backend\Forms'        => __DIR__ . '/../modules/backend/forms/',
    'Webshop\Models'               => __DIR__ . '/../common/models/',
    'Webshop'                      => __DIR__ . '/../library/'

));

$loader->register();