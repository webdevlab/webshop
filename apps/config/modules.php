<?php

/**
 * Register application modules
 */

$application->registerModules(array(
    'frontend' => array(
        'className' => 'Webshop\Frontend\Module',
        'path' => __DIR__ . '/../modules/frontend/Module.php'
    ),
    'backend' => array(
        'className' => 'Webshop\Backend\Module',
        'path' => __DIR__ . '/../modules/backend/Module.php'
    )
));
