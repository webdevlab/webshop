<?php

/**
 * Services are globally registered in this file
 */

use Phalcon\Mvc\Router,
    Phalcon\Mvc\View,
    Phalcon\Mvc\View\Engine\Volt as VoltEngine,
    Phalcon\Assets\Manager as AssetsManager,
    Phalcon\Mvc\Url as UrlResolver,
    Phalcon\DI\FactoryDefault,
    Phalcon\Session\Adapter\Files as SessionAdapter;

use Webshop\Auth\Auth;
use Webshop\Acl\Acl;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * Router
 */
$di->set(
    'router',
    function () {
        return include APP_PATH . "/apps/config/routers.php";
    },
    true
);

/**
 * The URL component is used to generate all kind of urls in the application
 */

$di->set(
    'url',
    function () {
        $url = new UrlResolver();
        $url->setBaseUri('/');

        return $url;
    },
    true
);

/**
 * Start the session the first time some component request the session service
 */
$di->set(
    'session',
    function () {
        $session = new SessionAdapter();

        $session->setOptions(array('lifetime' => 3600));

        $session->start();
        return $session;
    },
    true
);

/**
 * Setting up the view component
 */
$di['view'] = function(){
    $view = new View();

    $view->registerEngines(array(
        ".volt" => function($view, $di) {
                $volt = new VoltEngine($view, $di);

                $volt->setOptions(array(
                    "compiledPath" => APP_PATH ."/var/compiled-templates/",
                    "compiledSeparator" => "_",
                    "compiledExtension" => ".php",
                    "compileAlways"     => true
                ));

                return $volt;
            }
    ));

    return $view;
};

/**
 * Setting up the AssetsManager
 */
$di['assets'] = function() {

    $assets = new AssetsManager();

    $assets
        ->collection('header_css')
        ->addCss('//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css', false)
        ->addCss('css/justified-nav.css');

    $assets
        ->collection('header_js')
        ->addJs('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', false)
        ->addJs('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', false);

    return $assets;
};

//Для отображения ошибок (перебрасывания между контроллерами)
$di['flash'] = function() {
    return new Phalcon\Flash\Direct(array(
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning',
    ));
};

//Для отображения ошибок (перебрасывания между контроллерами)
$di['flashSession'] = function() {
    return new Phalcon\Flash\Session(array(
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning',
    ));
};

/**
 * Register the configuration itself as a service
 */
$di->set('config', $config);

/**
 * Main logger file
 */
$di->set('logger', function() {
    return new \Phalcon\Logger\Adapter\File(APP_PATH. '/var/logs/'.date('Y-m-d').'.log');
}, true);

/**
 * Error handler
 */
set_error_handler(function($errno, $errstr, $errfile, $errline) use ($di)
{
    if (!(error_reporting() & $errno)) {
        return;
    }

    $di->getFlash()->error($errstr);
    $di->getLogger()->log($errstr.' '.$errfile.':'.$errline, Phalcon\Logger::ERROR);

    return true;
});

/**
 * Custom authentication component
 */
$di->set('auth', function () {
    return new Auth();
});

/**
 * Access Control List
 */
$di->set('acl', function () {
    return new Acl();
});

$di->set('modelsManager', function() {
  return new Phalcon\Mvc\Model\Manager();
});

$di->setShared('transactions', function(){
    return new \Phalcon\Mvc\Model\Transaction\Manager();
});


