<?php
namespace Webshop\Auth;

use Phalcon\Mvc\User\Component,
    Webshop\Models\Users;

class Auth extends Component
{
    public function check($credentials)
    {

        // Check if the user exist
        $user = Users::findFirstByEmail($credentials['email']);
        if ($user == false) {
            $this->flashSession->error('Wrong email/password combination');
            return $this->response->redirect('admin');
        }

        // Check the password
        if (!$this->security->checkHash($credentials['password'], $user->password)) {
            $this->flashSession->error('Wrong email/password combination');
            return $this->response->redirect('admin');
        }

        $this->session->set('identity', array(
            'id' => $user->id,
            'name' => $user->username,
            'group' => $user->group->name
        ));
    }

    public function getIdentity($param = null)
    {
        $identity = $this->session->get('identity');
        if($param){
            switch($param){
                case 'id';
                case 'name';
                case 'group';
                    $result = $identity[$param];
                break;
                default;
                    $result = $identity;
                break;
            }
        }
        return $result;
    }

    public function getUsername()
    {
        $identity = $this->session->get('identity');
        return $identity['name'];
    }

    public function getUserGroup()
    {
        $identity = $this->session->get('identity');
        return $identity['group'];
    }

    public function remove()
    {
        $this->session->destroy();
    }
}