<?php
namespace Webshop\Acl;

use Phalcon\Mvc\User\Component;

class Acl extends Component
{
    private $privateResources = array(
        'users' => array(
            'index',
            'add'
        ),
        'categories' => array(
            'index',
            'add',
            'edit'
        ),
        'settings' => array(
            'index'
        ),
        'languages' => array(
            'index'
        )
    );

    public function isPrivate($controllerName, $actionName)
    {

        if(isset($this->privateResources[$controllerName])){
           if(in_array($actionName,$this->privateResources[$controllerName])){
               $private = true;
           } else {
               $private = false;
           }
        } else {
            $private = false;
        }
        return $private;
    }
}
 