<?php
namespace Webshop\Tool;

use Phalcon\Mvc\User\Component;

class ImageTool extends Component
{
    public static function resize($filename, $width, $height) {
        if (!is_file(APP_IMAGE_DIR . $filename)) {
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file(APP_IMAGE_DIR . $new_image) || (filectime(APP_IMAGE_DIR . $old_image) > filectime(APP_IMAGE_DIR . $new_image))) {
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(APP_IMAGE_DIR . $path)) {
                    @mkdir(APP_IMAGE_DIR . $path, 0777);
                }
            }

            list($width_orig, $height_orig) = getimagesize(APP_IMAGE_DIR . $old_image);

            if ($width_orig != $width || $height_orig != $height) {
                $image = new Image(APP_IMAGE_DIR . $old_image);
                $image->resize($width, $height);
                $image->save(APP_IMAGE_DIR . $new_image);
            } else {
                copy(APP_IMAGE_DIR . $old_image, APP_IMAGE_DIR . $new_image);
            }
        }

        return '/' . HTTP_IMAGE_DIR . $new_image;
    }
}
 