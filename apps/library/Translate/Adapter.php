<?php
namespace Webshop\Translate;

use Phalcon\Mvc\User\Component;

class Adapter extends Component
{
    protected $language;
    protected $lang_dir;

    public function __construct($options) {
        $this->language = $options['language'];
        $this->lang_dir = $options['lang_dir'];
    }

    public function load($file){
        $dir = $this->lang_dir . '/' . $this->language . '/' . $file;

        if (is_file($dir)) {
            $langFile = require_once($dir);
            return $langFile;
        } else {
            throw new \ErrorException('Не найден файл.');
        }

    }

}