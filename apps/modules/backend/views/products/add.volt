<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Добавить продукт</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url("admin/products") }}">Продукты</a></li>
        <li class="active">Добавить продукт</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="post">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general" data-toggle="tab">Основное</a></li>
                        <li><a href="#data" data-toggle="tab">Параметры</a></li>
                        <li class="pull-right">{{ form.render('Сохранить') }}</li>
                    </ul>
                    <div class="tab-content">
                            <div class="tab-pane nav-tabs-custom active" id="general">
                                <ul class="nav nav-tabs">
                                    {% for lang in languages %}
                                    <li{% if loop.first %} class="active"{% endif %}><a href="#lang{{ lang.lang_id }}" data-toggle="tab"><img src="/img/flags/{{ lang.image }}" title="{{ lang.name }}" /> {{ lang.name }}</a></li>
                                    {% endfor %}
                                </ul>
                                <div class="tab-content">
                                    <?php $index = 0; ?>
                                    <?php foreach($languages as $lang) { ?>
                                    <?php $index++; ?>
                                        <div class="tab-pane<?php if($index == 1){ echo " active"; } ?>" id="lang<?php echo $lang->lang_id; ?>">
                                            <div class="form-group">
                                                <?php echo $form->label("product_description[" . $lang->lang_id . "][name]", array('class'=>'col-sm-3 control-label')); ?>
                                                <div class="col-sm-9">
                                                    <?php echo $form->render("product_description[" . $lang->lang_id . "][name]") ?>
                                                    <?php echo $form->messages("product_description[" . $lang->lang_id . "][name]") ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->label("product_description[" . $lang->lang_id . "][meta_title]", array('class'=>'col-sm-3 control-label')); ?>
                                                <div class="col-sm-9">
                                                    <?php echo $form->render("product_description[" . $lang->lang_id . "][meta_title]") ?>
                                                    <?php echo $form->messages("product_description[" . $lang->lang_id . "][meta_title]") ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->label("product_description[" . $lang->lang_id . "][meta_description]", array('class'=>'col-sm-3 control-label')); ?>
                                                <div class="col-sm-9">
                                                    <?php echo $form->render("product_description[" . $lang->lang_id . "][meta_description]") ?>
                                                    <?php echo $form->messages("product_description[" . $lang->lang_id . "][meta_description]") ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->label("product_description[" . $lang->lang_id . "][meta_keyword]", array('class'=>'col-sm-3 control-label')); ?>
                                                <div class="col-sm-9">
                                                    <?php echo $form->render("product_description[" . $lang->lang_id . "][meta_keyword]") ?>
                                                    <?php echo $form->messages("product_description[" . $lang->lang_id . "][meta_keyword]") ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->label("product_description[" . $lang->lang_id . "][description]", array('class'=>'col-sm-3 control-label')); ?>
                                                <div class="col-sm-9">
                                                    <?php echo $form->render("product_description[" . $lang->lang_id . "][description]") ?>
                                                    <?php echo $form->messages("product_description[" . $lang->lang_id . "][description]") ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php echo $form->label("product_description[" . $lang->lang_id . "][tag]", array('class'=>'col-sm-3 control-label')); ?>
                                                <div class="col-sm-9">
                                                    <?php echo $form->render("product_description[" . $lang->lang_id . "][tag]") ?>
                                                    <?php echo $form->messages("product_description[" . $lang->lang_id . "][tag]") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div><!-- /.tab-pane -->
                            <div class="tab-pane" id="data">
                                <div class="form-group">
                                    {{ form.label('model', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('model') }}
                                        {{ form.messages('model') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('sku', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('sku') }}
                                        {{ form.messages('sku') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('upc', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('upc') }}
                                        {{ form.messages('upc') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('ean', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('ean') }}
                                        {{ form.messages('ean') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('jan', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('jan') }}
                                        {{ form.messages('jan') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('isbn', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('isbn') }}
                                        {{ form.messages('isbn') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('mpn', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('mpn') }}
                                        {{ form.messages('mpn') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('price', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('price') }}
                                        {{ form.messages('price') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('quantity', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('quantity') }}
                                        {{ form.messages('quantity') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('minimum', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('minimum') }}
                                        {{ form.messages('minimum') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        {{ form.render('shipping') }} {{ form.label('shipping', ['class':'control-label']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        {{ form.render('subtract') }} {{ form.label('subtract', ['class':'control-label']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        {{ form.render('status') }} {{ form.label('status', ['class':'control-label']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ form.label('sort_order', ['class':'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ form.render('sort_order') }}
                                        {{ form.messages('sort_order') }}
                                    </div>
                                </div>
                            </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->

</section><!-- /.content -->


