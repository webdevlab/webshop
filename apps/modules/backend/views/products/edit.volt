<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Редактировать категорию</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url("admin/categories") }}">Категории</a></li>
        <li class="active">Редактировать категорию</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="post">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general" data-toggle="tab">Основное</a></li>
                        <li><a href="#data" data-toggle="tab">Параметры</a></li>
                        <li class="pull-right">{{ form.render('Сохранить') }}</li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane nav-tabs-custom active" id="general">
                            <ul class="nav nav-tabs">
                                {% for lang in languages %}
                                <li{% if loop.first %} class="active"{% endif %}><a href="#lang{{ lang.lang_id }}" data-toggle="tab"><img src="/img/flags/{{ lang.image }}" title="{{ lang.name }}" /> {{ lang.name }}</a></li>
                                {% endfor %}
                            </ul>
                            <div class="tab-content">
                                <?php $index = 0; ?>
                                <?php foreach($languages as $lang) { ?>
                                    <?php $index++; ?>
                                    <div class="tab-pane<?php if($index == 1){ echo " active"; } ?>" id="lang<?php echo $lang->lang_id; ?>">
                                        <div class="form-group">
                                            <?php echo $form->label("category_description[" . $lang->lang_id . "][name]", array('class'=>'col-sm-3 control-label')); ?>
                                            <div class="col-sm-9">
                                                <?php echo $form->render("category_description[" . $lang->lang_id . "][name]") ?>
                                                <?php echo $form->messages("category_description[" . $lang->lang_id . "][name]") ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->label("category_description[" . $lang->lang_id . "][meta_title]", array('class'=>'col-sm-3 control-label')); ?>
                                            <div class="col-sm-9">
                                                <?php echo $form->render("category_description[" . $lang->lang_id . "][meta_title]") ?>
                                                <?php echo $form->messages("category_description[" . $lang->lang_id . "][meta_title]") ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->label("category_description[" . $lang->lang_id . "][meta_description]", array('class'=>'col-sm-3 control-label')); ?>
                                            <div class="col-sm-9">
                                                <?php echo $form->render("category_description[" . $lang->lang_id . "][meta_description]") ?>
                                                <?php echo $form->messages("category_description[" . $lang->lang_id . "][meta_description]") ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->label("category_description[" . $lang->lang_id . "][meta_keyword]", array('class'=>'col-sm-3 control-label')); ?>
                                            <div class="col-sm-9">
                                                <?php echo $form->render("category_description[" . $lang->lang_id . "][meta_keyword]") ?>
                                                <?php echo $form->messages("category_description[" . $lang->lang_id . "][meta_keyword]") ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->label("category_description[" . $lang->lang_id . "][description]", array('class'=>'col-sm-3 control-label')); ?>
                                            <div class="col-sm-9">
                                                <?php echo $form->render("category_description[" . $lang->lang_id . "][description]") ?>
                                                <?php echo $form->messages("category_description[" . $lang->lang_id . "][description]") ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="data">
                            <div class="form-group">
                                {{ form.label('parent_id', ['class':'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ form.render('parent_id') }}
                                    {{ form.messages('parent_id') }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ form.label('sort_order', ['class':'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ form.render('sort_order') }}
                                    {{ form.messages('sort_order') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    {{ form.render('status') }} {{ form.label('status', ['class':'control-label']) }}
                                </div>
                            </div>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->

</section><!-- /.content -->

