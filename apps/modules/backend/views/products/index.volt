<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Продукты</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Продукты</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    <h4 class="page-header">
        <a class="btn bg-olive" href="{{ url('admin/products/add') }}" role="button">Добавить продукт</a><br>
    </h4>

    {% for product in products %}
    {% if loop.first %}
    <div class="table-responsive">
        <table id="products-table" class="table table-hover table-bordered">
            <tr class="success">
                <th>#</th>
                <th>ID</th>
                <th>Название</th>
            </tr>
            {% endif %}
            <tr class="trow" data-pid="{{ product.data.id }}">
                <td>{{ loop.index }}</td>
                <td>{{ product.data.id }}</td>
                <td>{{ product.description.name }}</td>
            </tr>
            {% if loop.last %}
        </table>
    </div>
    {% endif %}
    {% endfor %}

    <script>
        $(document).ready(function(){
            $("#products-table .trow").click(function(){
                window.location = '{{ url('admin/products/') }}' + $(this).data('pid');
            });
        });
    </script>
</section><!-- /.content -->

