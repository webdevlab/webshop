<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Настройки</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Настройки</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    {% for setting in settings %}
        {% if setting.value is type('array') %}
            {{ setting.key }}:  <br>
            <ul>
                {% for key, value in setting.value[0] %}
                <li>{{ key }}: {{ value }}</li>
                {% endfor %}
            </ul>
        {% else%}
            {{ setting.key }}: {{ setting.value }}<br>
        {% endif %}
    {% endfor %}
</section><!-- /.content -->