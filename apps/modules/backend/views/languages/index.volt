<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Языки</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Система</li>
        <li class="active">Языки</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    <!--<h4 class="page-header">
        <a class="btn bg-olive" href="{{ url('admin/languages/add') }}" role="button">Добавить язык</a><br>
    </h4>-->

    {% for lang in languages %}
    {% if loop.first %}
    <div class="table-responsive">
        <table id="lang-table" class="table table-hover table-bordered">
            <tr class="success">
                <th>#</th>
                <th>Язык</th>
            </tr>
            {% endif %}
            <tr class="trow" data-uid="{{ lang.lang_id }}">
                <td>{{ loop.index }}</td>
                <td>{{ lang.name }}</td>
            </tr>
            {% if loop.last %}
        </table>
    </div>
    {% endif %}
    {% endfor %}

</section><!-- /.content -->

