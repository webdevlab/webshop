<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Категории</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Категории</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    <h4 class="page-header">
        <a class="btn bg-olive" href="{{ url('admin/categories/add') }}" role="button">Добавить категорию</a><br>
    </h4>

    {% for category in categories %}
    {% if loop.first %}
    <div class="table-responsive">
        <table id="categories-table" class="table table-hover table-bordered">
            <tr class="success">
                <th>#</th>
                <th>ID</th>
                <th>Название</th>
            </tr>
            {% endif %}
            <tr class="trow" data-cid="{{ category.data.id }}">
                <td>{{ loop.index }}</td>
                <td>{{ category.data.id }}</td>
                <td>{{ category.description.name }}</td>
            </tr>
            {% if loop.last %}
        </table>
    </div>
    {% endif %}
    {% endfor %}

    <script>
        $(document).ready(function(){
            $("#categories-table .trow").click(function(){
                window.location = '{{ url('admin/categories/') }}' + $(this).data('cid');
            });
        });
    </script>
</section><!-- /.content -->

