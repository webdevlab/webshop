<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>{{ user.username }}<small>({{ user.getGroup().name }})</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url("admin/users") }}">Пользователи</a></li>
        <li class="active">Страница пользователя</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    {{ content() }}

    <div>
        Username: {{ user.username }} <br>
        Email:  {{ user.email }} <br>
        Группа: {{ user.getGroup().name }}
    </div>
</section><!-- /.content -->

