<div class="form-box" id="login-box">
    {{ content() }}
    <?php $this->flashSession->output() ?>
    <div class="header">Вход</div>
    {{ form() }}
        <div class="body bg-gray">
            <div class="form-group">{{ form.render('email') }}</div>
            <div class="form-group">{{ form.render('password') }}</div>
            <div class="form-group">{{ form.render('csrf', ['value': security.getToken()]) }}</div>
        </div>
        <div class="footer">
            {{ form.render('Login') }}
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $('html').addClass('bg-black');
    });
</script>