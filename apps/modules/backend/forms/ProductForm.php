<?php
namespace Webshop\Backend\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\TextArea,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Submit,
    Phalcon\Validation\Validator\PresenceOf,
    Webshop\Models\Languages;

class ProductForm extends Form
{
    protected $edit;
    public function initialize($entity = null, $options = null)
    {
        $languages = Languages::find('status = 1');

        if (isset($options['edit']) && $options['edit']) {
            $this->edit = true;
            $id = new Hidden('id');
        } else {
            $this->edit = false;
            $id = new Text('id');
        }
        $this->add($id);

        foreach($languages as $lang){
            if($this->edit) {
                $pDescription = $entity->getDescription('lang_id=' . $lang->lang_id)->getFirst();
            }
            $name = new Text('product_description[' . $lang->lang_id . '][name]', array('class' => 'form-control'));
            $name->addValidator(
                new PresenceOf(array('message' => 'Название продукта обязательно для заполнения')
            ));
            $name->setLabel('Название продукта:<span class="require">*</span>');
            if($this->edit){
                $name->setDefault($pDescription->name);
            };
            $this->add($name);

            $meta_title = new Text('product_description[' . $lang->lang_id . '][meta_title]', array('class' => 'form-control'));
            $meta_title->setLabel('Мета-тег "Заголовок":');
            if($this->edit) {
                $meta_title->setDefault($pDescription->meta_title);
            }
            $this->add($meta_title);

            $meta_description = new Text('product_description[' . $lang->lang_id . '][meta_description]', array('class' => 'form-control'));
            $meta_description->setLabel('Мета-тег "Описание":');
            if($this->edit) {
                $meta_description->setDefault($pDescription->meta_description);
            }
            $this->add($meta_description);

            $meta_keyword = new Text('product_description[' . $lang->lang_id . '][meta_keyword]', array('class' => 'form-control'));
            $meta_keyword->setLabel('Мета-тег "Ключевые слова":');
            if($this->edit) {
                $meta_keyword->setDefault($pDescription->meta_keyword);
            }
            $this->add($meta_keyword);

            $description = new TextArea('product_description[' . $lang->lang_id . '][description]', array('class' => 'form-control'));
            $description->setLabel('Описание:');
            if($this->edit) {
                $description->setDefault($pDescription->description);
            }
            $this->add($description);

            $tag = new Text('product_description[' . $lang->lang_id . '][tag]', array('class' => 'form-control'));
            $tag->setLabel('Теги товара:');
            if($this->edit) {
                $tag->setDefault($pDescription->tag);
            }
            $this->add($tag);
        }

        /*$manufacturer = new Select('parent_id', Manufacturer::find(), array(
            'class' => 'form-control',
            'using' => array(
                'id',
                'name'
            ),
            'useEmpty' => true,
            'emptyText' => 'Выберите производителя',
            'emptyValue' => '0'
        ));
        $manufacturer->setLabel('Производитель');
        $this->add($manufacturer); */

        $model = new Text('model', array('class' => 'form-control'));
        $model->addValidator(
            new PresenceOf(array('message' => 'Модель продукта обязательно для заполнения')
            ));
        $model->setLabel('Модель:<span class="require">*</span>');
        $this->add($model);

        // Коди
        $sku = new Text('sku', array('class' => 'form-control'));
        $sku->setLabel('Артикул (SKU, код производителя)');
        $this->add($sku);

        $upc = new Text('upc', array('class' => 'form-control'));
        $upc->setLabel('UPC');
        $this->add($upc);

        $ean = new Text('ean', array('class' => 'form-control'));
        $ean->setLabel('EAN');
        $this->add($ean);

        $jan = new Text('jan', array('class' => 'form-control'));
        $jan->setLabel('JAN');
        $this->add($jan);

        $isbn = new Text('isbn', array('class' => 'form-control'));
        $isbn->setLabel('ISBN');
        $this->add($isbn);

        $mpn = new Text('mpn', array('class' => 'form-control'));
        $mpn->setLabel('MPN');
        $this->add($mpn);

        $price = new Text('price', array('class' => 'form-control'));
        $price->setLabel('Цена');
        $this->add($price);

        $quantity = new Text('quantity', array('class' => 'form-control'));
        $quantity->setLabel('Количество');
        $this->add($quantity);

        $minimum = new Text('minimum', array('class' => 'form-control'));
        $minimum->setLabel('Минимальное количество');
        $this->add($minimum);

        $shipping = new Check('shipping');
        $shipping->setLabel('Доставка');
        $this->add($shipping);

        $subtract = new Check('subtract');
        $subtract->setLabel('Вычитывать со склада');
        $this->add($subtract);

        $status = new Check('status');
        $status->setLabel('Продукт активный');
        $this->add($status);

        $sort_order = new Numeric('sort_order', array('class' => 'form-control'));
        $sort_order->setLabel('Порядок сортировки:');
        $this->add($sort_order);

        $this->add(new Submit('Сохранить', array(
            'class' => 'btn btn-success btn-sm'
        )));

    }

    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

}