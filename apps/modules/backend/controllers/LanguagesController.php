<?php

namespace Webshop\Backend\Controllers;

use Phalcon\Tag,
    Webshop\Models\Languages;

class LanguagesController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->languages = Languages::find();
    }

}

