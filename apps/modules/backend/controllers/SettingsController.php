<?php

namespace Webshop\Backend\Controllers;

use Phalcon\Tag,
    Webshop\Models\Settings;

class SettingsController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->settings = Settings::find("type='system'");
    }

}

