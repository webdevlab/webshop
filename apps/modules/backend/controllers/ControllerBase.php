<?php

namespace Webshop\Backend\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{
    protected $lang_id;
    public function initialize()
    {
        $this->view->setTemplateBefore('main');
        $this->lang_id = 1;
    }

    public function getLangId(){
        return $this->lang_id;
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();

        // Only check permissions on private controllers
        if ($this->acl->isPrivate($controllerName, $actionName)) {

            // Get the current identity
            $identity = $this->session->get('identity');

            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity) || ($identity['group']!= 'Administrator')) {

                $this->flash->notice('You don\'t have access to this module: private');
                $this->session->set('auth_redirect', $this->router->getRewriteUri());

                $dispatcher->forward(array(
                    'controller' => 'index',
                    'action' => 'index'
                ));
                return false;
            }
        } 
    }

    public function preparePostForValidation($post, $level_key = null){
        $flat_array = array();
        foreach($post as $key=>$value){
            if(is_array($value)){
                $key = ($level_key) ? $level_key . '[' . $key . ']'  : $key;
                $flat_array = array_merge($flat_array, $this->preparePostForValidation($value, $key));
            } else {
                if($level_key){
                    $flat_array[$level_key.'['.$key.']'] = $value;
                } else {
                    $flat_array[$key] = $value;
                }
            }
        }

        return $flat_array;
    }

}
