<?php

namespace Webshop\Backend\Controllers;

use Phalcon\Tag,
    Webshop\Models\Languages,
    Webshop\Models\Products,
    Webshop\Models\ProductsDescription,
    Webshop\Backend\Forms\ProductForm,
    Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class ProductsController extends ControllerBase
{

    public function indexAction()
    {
      $this->view->products = Products::findWithDescription($this->modelsManager, $this->lang_id);
    }

    public function addAction()
    {
        $languages = Languages::find('status = 1');
        $this->view->languages = $languages;
        $form = new ProductForm(null);
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($form->isValid($this->preparePostForValidation($this->request->getPost()))) {
                try {
                    $transaction = $this->transactions->get();

                    $shipping = $this->request->getPost('shipping');
                    $subtract = $this->request->getPost('subtract');
                    $status = $this->request->getPost('status');

                    // Создание продукта
                    $product = new Products();
                    $product->setTransaction($transaction);
                    $product->model = $this->request->getPost('model');
                    $product->sku = $this->request->getPost('sku');
                    $product->upc = $this->request->getPost('upc');
                    $product->ean = $this->request->getPost('ean');
                    $product->jan = $this->request->getPost('jan');
                    $product->isbn = $this->request->getPost('isbn');
                    $product->mpn = $this->request->getPost('mpn');
                    $product->quantity = $this->request->getPost('quantity', 'int');
                    $product->minimum = $this->request->getPost('minimum');
                    $product->price = $this->request->getPost('price');
                    $product->stock_status_id = 1;
                    $product->manufacturer_id = 1;
                    $product->available_at = time();
                    $product->shipping = (isset($shipping)) ? 1 : 0;
                    $product->subtract = (isset($subtract)) ? 1 : 0;
                    $product->status = (isset($status)) ? 1 : 0;
                    $product->sort_order = $this->request->getPost('sort_order', 'int');

                    // Сохранение продукта
                    if ($product->save() == false) {
                        foreach ($product->getMessages() as $message) {
                            $transaction->rollback($message->getMessage());
                        }
                    }

                    // Создание описания для каждого активного языка
                    $description = array();
                    foreach($languages as $lang){
                        $lang_id = $lang->lang_id;
                        $description[$lang_id] = new ProductsDescription();
                        $description[$lang_id]->setTransaction($transaction);
                        $description[$lang_id]->lang_id = $lang_id;
                        $description[$lang_id]->product_id = $product->id; // Назначение связи продукта
                        $description[$lang_id]->name = $this->request->getPost('product_description')[$lang_id]['name'];
                        $description[$lang_id]->description = $this->request->getPost('product_description')[$lang_id]['description'];
                        $description[$lang_id]->meta_title = $this->request->getPost('product_description')[$lang_id]['meta_title'];
                        $description[$lang_id]->meta_description = $this->request->getPost('product_description')[$lang_id]['meta_description'];
                        $description[$lang_id]->meta_keyword = $this->request->getPost('product_description')[$lang_id]['meta_keyword'];
                        $description[$lang_id]->tag = $this->request->getPost('product_description')[$lang_id]['tag'];
                        if($description[$lang->lang_id]->save() == false){
                            foreach ($description[$lang->lang_id]->getMessages() as $message) {
                                $transaction->rollback($message->getMessage());
                            }
                        }
                    }

                    $transaction->commit();
                    $this->flash->success("Продукт был успешно сохранен!");
                    return $this->dispatcher->forward(array(
                        'action' => 'index'
                    ));

                } catch(TxFailed $e) {
                    echo "Failed, reason: ", $e->getMessage();
                }

            }
        }

    }

    public function editAction($cid)
    {
        $category = Categories::findFirst($cid);

        if (!$category) {
            $this->flash->error("Категория не найдена!");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        $languages = Languages::find('status = 1');
        $this->view->languages = $languages;
        $form = new CategoryForm($category, array(
            'edit' => true
        ));

        if ($this->request->isPost()) {
            if ($form->isValid($this->preparePostForValidation($this->request->getPost()))) {
                try {
                    $transaction = $this->transactions->get();

                    $status = $this->request->getPost('status');

                    $category->setTransaction($transaction);
                    $category->parent_id = $this->request->getPost('parent_id', 'int');
                    $category->sort_order = $this->request->getPost('sort_order', 'int');
                    $category->status = (isset($status)) ? 1 : 0;

                    // Обновление категории
                    if ($category->save() == false) {
                        $transaction->rollback("Неудалось обновить категорию!");
                    }

                    // Обновление описания для каждого активного языка
                    $description = array();
                    foreach($languages as $lang){
                        $lang_id = $lang->lang_id;
                        $description[$lang_id] = new CategoriesDescription(array('category_id'=>$category->id, "lang_id"=>$lang_id));
                        $description[$lang_id]->setTransaction($transaction);
                        $description[$lang_id]->lang_id = $lang_id;
                        $description[$lang_id]->category_id = $category->id; // Назначение связи категории
                        $description[$lang_id]->name = $this->request->getPost('category_description')[$lang_id]['name'];
                        $description[$lang_id]->description = $this->request->getPost('category_description')[$lang_id]['description'];
                        $description[$lang_id]->meta_title = $this->request->getPost('category_description')[$lang_id]['meta_title'];
                        $description[$lang_id]->meta_description = $this->request->getPost('category_description')[$lang_id]['meta_description'];
                        $description[$lang_id]->meta_keyword = $this->request->getPost('category_description')[$lang_id]['meta_keyword'];
                        if($description[$lang->lang_id]->save() == false){
                            $transaction->rollback("Неудалось обновить описание категории!");
                        }
                    }

                    $transaction->commit();
                    $this->flash->success("Категория была успешно обновлена!");
                    return $this->dispatcher->forward(array(
                        'action' => 'index'
                    ));

                } catch(TxFailed $e) {
                    echo "Failed, reason: ", $e->getMessage();
                }
            }

        }

        $this->view->form = $form;
    }

    public function deleteAction($cid)
    {
        $category = Categories::findFirst($cid);

        if (!$category) {
            $this->flash->error("Категория не найдена!");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        } else {
            try {
                $transaction = $this->transaction->get();

                foreach ($category->getDescription() as $category_description) {
                    $category_description->setTransaction($transaction);
                    if ($category_description->delete() == false) {
                        foreach ($category_description->getMessages() as $message) {
                            $transaction->rollback($message->getMessage());
                        }
                    }
                }

                $category->setTransaction($transaction);
                if ($category->delete() == false) {
                    foreach ($category->getMessages() as $message) {
                        $transaction->rollback($message->getMessage());
                    }
                }

                $transaction->commit();
                $this->flash->success("Категория была успешно удалена!");
                return $this->dispatcher->forward(array(
                    'action' => 'index'
                ));

            } catch(TxFailed $e) {
                echo "Failed, reason: ", $e->getMessage();
            }
        }
    }

}

