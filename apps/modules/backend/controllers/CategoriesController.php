<?php

namespace Webshop\Backend\Controllers;

use Phalcon\Tag,
    Webshop\Models\Categories,
    Webshop\Models\Languages,
    Webshop\Backend\Forms\CategoryForm,
    Webshop\Models\CategoriesDescription,
    Phalcon\Mvc\Model\Transaction\Failed as TxFailed;

class CategoriesController extends ControllerBase
{

    public function indexAction()
    {
      $this->view->categories = Categories::findWithDescription($this->modelsManager, $this->lang_id);
    }

    public function addAction()
    {
        $this->assets->collection('admin_header_js')
            ->addJs('js/ckeditor/ckeditor.js');
        $languages = Languages::find('status = 1');
        $this->view->languages = $languages;
        $form = new CategoryForm(null);
        //TODO: загружать изображение указаное в конфигурации если не задано для категории изображения;
        $this->view->category_image = '/' . HTTP_IMAGE_DIR . '/no_image.jpg';
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($form->isValid($this->preparePostForValidation($this->request->getPost()))) {
                try {
                    $transaction = $this->transactions->get();

                    $status = $this->request->getPost('status');

                    // Создание категории
                    $category = new Categories();
                    $category->setTransaction($transaction);
                    $category->image = str_replace('/' . HTTP_IMAGE_DIR,'',$this->request->getPost('category_image', 'string'));
                    $category->parent_id = $this->request->getPost('parent_id', 'int');
                    $category->sort_order = $this->request->getPost('sort_order', 'int');
                    $category->status = (isset($status)) ? 1 : 0;

                    // Сохранение категории
                    if ($category->save() == false) {
                        foreach ($category->getMessages() as $message) {
                            $transaction->rollback($message->getMessage());
                        }
                        //$transaction->rollback("Невозможно сохранить категорию!");
                    }

                    // Создание описания для каждого активного языка
                    $description = array();
                    foreach($languages as $lang){
                        $lang_id = $lang->lang_id;
                        $description[$lang_id] = new CategoriesDescription();
                        $description[$lang_id]->setTransaction($transaction);
                        $description[$lang_id]->lang_id = $lang_id;
                        $description[$lang_id]->category_id = $category->id; // Назначение связи категории
                        $description[$lang_id]->name = $this->request->getPost('category_description')[$lang_id]['name'];
                        $description[$lang_id]->description = $this->request->getPost('category_description')[$lang_id]['description'];
                        $description[$lang_id]->meta_title = $this->request->getPost('category_description')[$lang_id]['meta_title'];
                        $description[$lang_id]->meta_description = $this->request->getPost('category_description')[$lang_id]['meta_description'];
                        $description[$lang_id]->meta_keyword = $this->request->getPost('category_description')[$lang_id]['meta_keyword'];
                        if($description[$lang->lang_id]->save() == false){
                            foreach ($description[$lang->lang_id]->getMessages() as $message) {
                                $transaction->rollback($message->getMessage());
                            }
                            //$transaction->rollback("Невозможно сохранить описание категории!");
                        }
                    }

                    $transaction->commit();
                    $this->flash->success("Категория была успешно создана!");
                    return $this->dispatcher->forward(array(
                        'action' => 'index'
                    ));

                } catch(TxFailed $e) {
                    echo "Failed, reason: ", $e->getMessage();
                }

            }
        }

    }

    public function editAction($cid)
    {
        $this->assets->collection('admin_header_js')
            ->addJs('js/ckeditor/ckeditor.js');
        $category = Categories::findFirst($cid);

        if (!$category) {
            $this->flash->error("Категория не найдена!");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }
        //TODO: загружать изображение указаное в конфигурации если не задано для категории изображения;
        $this->view->category_image = ($category->image)?'/' . HTTP_IMAGE_DIR . $category->image:'/' . HTTP_IMAGE_DIR . '/no_image.jpg';

        $languages = Languages::find('status = 1');
        $this->view->languages = $languages;
        $form = new CategoryForm($category, array(
            'edit' => true
        ));

        if ($this->request->isPost()) {
            if ($form->isValid($this->preparePostForValidation($this->request->getPost()))) {
                try {
                    $transaction = $this->transactions->get();

                    $status = $this->request->getPost('status');

                    $category->setTransaction($transaction);
                    $category->image = str_replace('/' . HTTP_IMAGE_DIR,'',$this->request->getPost('category_image', 'string'));
                    $category->parent_id = $this->request->getPost('parent_id', 'int');
                    $category->sort_order = $this->request->getPost('sort_order', 'int');
                    $category->status = (isset($status)) ? 1 : 0;

                    // Обновление категории
                    if ($category->save() == false) {
                        $transaction->rollback("Неудалось обновить категорию!");
                    }

                    // Обновление описания для каждого активного языка
                    $description = array();
                    foreach($languages as $lang){
                        $lang_id = $lang->lang_id;
                        $description[$lang_id] = new CategoriesDescription(array('category_id'=>$category->id, "lang_id"=>$lang_id));
                        $description[$lang_id]->setTransaction($transaction);
                        $description[$lang_id]->lang_id = $lang_id;
                        $description[$lang_id]->category_id = $category->id; // Назначение связи категории
                        $description[$lang_id]->name = $this->request->getPost('category_description')[$lang_id]['name'];
                        $description[$lang_id]->description = $this->request->getPost('category_description')[$lang_id]['description'];
                        $description[$lang_id]->meta_title = $this->request->getPost('category_description')[$lang_id]['meta_title'];
                        $description[$lang_id]->meta_description = $this->request->getPost('category_description')[$lang_id]['meta_description'];
                        $description[$lang_id]->meta_keyword = $this->request->getPost('category_description')[$lang_id]['meta_keyword'];
                        if($description[$lang->lang_id]->save() == false){
                            $transaction->rollback("Неудалось обновить описание категории!");
                        }
                    }

                    $transaction->commit();
                    $this->flash->success("Категория была успешно обновлена!");
                    return $this->dispatcher->forward(array(
                        'action' => 'index'
                    ));

                } catch(TxFailed $e) {
                    echo "Failed, reason: ", $e->getMessage();
                }
            }

        }

        $this->view->form = $form;
    }

    public function deleteAction($cid)
    {
        $category = Categories::findFirst($cid);

        if (!$category) {
            $this->flash->error("Категория не найдена!");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        } else {
            try {
                $transaction = $this->transaction->get();

                foreach ($category->getDescription() as $category_description) {
                    $category_description->setTransaction($transaction);
                    if ($category_description->delete() == false) {
                        foreach ($category_description->getMessages() as $message) {
                            $transaction->rollback($message->getMessage());
                        }
                    }
                }

                $category->setTransaction($transaction);
                if ($category->delete() == false) {
                    foreach ($category->getMessages() as $message) {
                        $transaction->rollback($message->getMessage());
                    }
                }

                $transaction->commit();
                $this->flash->success("Категория была успешно удалена!");
                return $this->dispatcher->forward(array(
                    'action' => 'index'
                ));

            } catch(TxFailed $e) {
                echo "Failed, reason: ", $e->getMessage();
            }
        }
    }

}

