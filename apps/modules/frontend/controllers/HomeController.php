<?php

namespace Webshop\Frontend\Controllers;

class HomeController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->lang = $this->request->getBestLanguage();
    }

}

