<?php

namespace Webshop\Frontend\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    protected $lang_id;
    public function initialize()
    {
        $this->view->setTemplateBefore('main');
        $this->tag->setTitle("Webshop on Phalcon PHP (" . APPLICATION_ENV . ")");
        $this->lang_id = 1;
    }

    public function getLangId(){
        return $this->lang_id;
    }

}
