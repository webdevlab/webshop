<?php

namespace Webshop\Frontend\Controllers;

use Webshop\Models\Categories;
use Webshop\Tool\ImageTool;

class CategoriesController extends ControllerBase
{

    public function indexAction()
    {
            $this->view->categories = Categories::findWithDescription(
                $this->modelsManager,
                $this->getLangId(),
                array(
                    'parent_id' => 0,
                    'status' => 1
                )
            );

    }

    public function showAction($id)
    {
         $category = Categories::findWithDescription(
            $this->modelsManager,
            $this->getLangId(),
            array(
                'id' => $id,
                'status' => 1
            )
        );

        if($category->count()){
            $this->view->category = $category->getFirst();
            if($this->view->category->data->image){
                //TODO: получать размеры изображений для категорий из конфигурации
                $this->view->category_image = ImageTool::resize($this->view->category->data->image, 150, 150);
            } else {
                $this->view->category_image = '';
            }
            $this->view->category_child = Categories::findWithDescription(
                $this->modelsManager,
                $this->getLangId(),
                array(
                    'parent_id' => $id,
                    'status' => 1
                )
            );
        } else {
            return $this->dispatcher->forward(array(
                'controller' => 'error',
                'action' => 'show404',
                'params' => array(
                    'msg' => 'Категория не найдена!'
                )
            ));
        }

    }

}

