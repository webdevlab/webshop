<?php

namespace Webshop\Frontend\Controllers;

use Webshop\Models\Users;

class UsersController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->users = Users::find();

    }

}

