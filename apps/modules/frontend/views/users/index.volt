<h1>Пользователи</h1>

{% for user in users %}
{% if loop.first %}
<div class="table-responsive">
    <table id="user-table" class="table table-hover table-bordered">
    <tr class="success">
        <th>#</th>
        <th>Id</th>
        <th>Username</th>
        <th>Email</th>
        <th>Група</th>
    </tr>
{% endif %}
    <tr class="trow" data-uid="{{ user.id }}">
        <td>{{ loop.index }}</td>
        <td>{{ user.id }}</td>
        <td>{{ user.username }}</td>
        <td>{{ user.email }}</td>
        <td>{{ user.getGroup().name }}</td>
    </tr>
{% if loop.last %}
    </table>
</div>
{% endif %}
{% endfor %}