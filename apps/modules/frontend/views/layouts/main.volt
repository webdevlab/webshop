{{ partial("partials/head") }}
<body>
    <div class="container">
        {{ partial("partials/header") }}
        <div class="row">
            <div class="col-md-12">
                {{ content() }}
            </div>
        </div>
        {{ partial("partials/footer") }}
    </div>
</body>