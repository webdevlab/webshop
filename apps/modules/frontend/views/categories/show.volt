<section>
    <header><h1>{{ category.description.name }}</h1></header>
    {% if category_image %}
    <img src="{{ category_image }}" alt="{{ category.description.name }}"/>
    {% endif %}
    {% if category.description.description %}
    <section class="row">
        <div class="col-md-12">
            {{ category.description.description }}
        </div>
    </section>
    {% endif %}
    {% if category_child.count() %}
    <section>
        <h2>Подкатегории</h2>
        <section>
            {% for child in category_child %}
            <figure class="col-md-3 col-sm-12">
                {% if child.data.image %}
                    <img src="{{ child.data.image }}">
                {% endif %}

                <figcaption><a href="{{ url("categories/"~child.data.id) }}">{{ child.description.name }}</a></figcaption>
            </figure>
            {% endfor %}
        </section>
    </section>
    {% endif %}
</section>