<h1>Категории</h1>
<ul>
{% for category in categories %}
  <li><a href="{{ url("categories/"~category.data.id) }}">{{ category.description.name }}</a></li>
{% endfor %}
</ul>