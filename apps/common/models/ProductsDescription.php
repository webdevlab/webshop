<?php

namespace Webshop\Models;

class ProductsDescription extends \Phalcon\Mvc\Model
{
    public $product_id;
    public $lang_id;
    public $name;
    public $description;
    public $meta_title;
    public $meta_description;
    public $meta_keyword;
    public $tag;

    public function initialize()
    {
        $this->setSource("product_description");

        $this->belongsTo('product_id', 'Webshop\Models\Products', 'id', array(
            'alias' => 'description',
            'reusable' => true
        ));
    }

}