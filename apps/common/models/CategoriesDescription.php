<?php

namespace Webshop\Models;

class CategoriesDescription extends \Phalcon\Mvc\Model
{
    public $category_id;
    public $lang_id;
    public $name;
    public $description;
    public $meta_title;
    public $meta_description;
    public $meta_keyword;

    public function initialize()
    {
        $this->setSource("category_description");

        $this->belongsTo('category_id', 'Webshop\Models\Categories', 'id', array(
            'alias' => 'description',
            'reusable' => true
        ));
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLangId()
    {
        return $this->lang_id;
    }

    /**
     * @param mixed $lang_id
     */
    public function setLangId($lang_id)
    {
        $this->lang_id = $lang_id;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * @param mixed $meta_description
     */
    public function setMetaDescription($meta_description)
    {
        $this->meta_description = $meta_description;
    }

    /**
     * @return mixed
     */
    public function getMetaKeyword()
    {
        return $this->meta_keyword;
    }

    /**
     * @param mixed $meta_keyword
     */
    public function setMetaKeyword($meta_keyword)
    {
        $this->meta_keyword = $meta_keyword;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * @param mixed $meta_title
     */
    public function setMetaTitle($meta_title)
    {
        $this->meta_title = $meta_title;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}