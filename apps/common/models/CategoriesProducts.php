<?php

namespace Webshop\Models;

class CategoriesProducts extends \Phalcon\Mvc\Model
{
    public $product_id;
    public $category_id;

    public function initialize()
    {
        $this->setSource("product_to_category");

        $this->belongsTo('category_id', 'Webshop\Models\Categories', 'id',
            array('alias' => 'category')
        );

        $this->belongsTo('product_id', 'Webshop\Models\Products', 'id',
            array('alias' => 'product')
        );

    }

    /**
     * @param mixed $category_id
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }



}