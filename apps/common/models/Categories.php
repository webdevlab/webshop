<?php

namespace Webshop\Models;

class Categories extends \Phalcon\Mvc\Model
{
    public $id;
    public $image;
    public $parent_id;
    public $sort_order;
    public $status;
    public $created_at;
    public $modified_at;

    public function initialize()
    {
        $this->setSource("category");

        $this->hasMany('id', 'Webshop\Models\CategoriesDescription', 'category_id', array(
            'alias' => 'description',
            'reusable' => true
        ));

        $this->hasMany('id', 'Webshop\Models\Categories', 'parent_id', array(
            'alias' => 'child',
            'reusable' => true
        ));

        $this->belongsTo('parent_id', 'Webshop\Models\Categories', 'id', array(
            'alias' => 'parent',
            'reusable' => true
        ));

        $this->hasManyToMany(
            'id',
            'Webshop\Models\CategoriesProducts',
            'category_id',
            'product_id',
            'Webshop\Models\Products',
            'id',
            array('alias' => 'products')
        );
    }

    public static function findWithDescription($modelsManager, $langId, $params=null) {
        $builder = $modelsManager->createBuilder();
        $builder->columns('data.*, description.*')
            ->addFrom('Webshop\Models\Categories', 'data')
            ->join('Webshop\Models\CategoriesDescription', 'data.id = description.category_id', 'description')
            ->where('description.lang_id = :langId:', array('langId' => $langId));
        if($params){
            foreach ($params as $key => $value) {
                $builder->andWhere('data.' . $key . ' = :' . $key . ':', array($key => $value));
            }
        }
        return $builder->getQuery()->execute();
    }

    public function beforeValidationOnCreate()
    {
        $this->created_at = time();
    }

    public function beforeValidationOnUpdate()
    {
        $this->modified_at = time();
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * @param mixed $modified_at
     */
    public function setModifiedAt($modified_at)
    {
        $this->modified_at = $modified_at;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->sort_order;
    }

    /**
     * @param mixed $sort_order
     */
    public function setSortOrder($sort_order)
    {
        $this->sort_order = $sort_order;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }



}