<?php
namespace Webshop\Models;

use Phalcon\Mvc\Model;

class Settings extends Model
{

    /**
     *
     * @var string
     */
    public $key;

    /**
     *
     * @var string
     */
    public $value;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var int
     */
    public $serialized;

    public function initialize()
    {
        $this->setSource("setting");
    }

    public function afterFetch()
    {
        $this->value = ($this->serialized)?unserialize($this->value):$this->value;
    }

}