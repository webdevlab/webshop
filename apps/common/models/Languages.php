<?php
namespace Webshop\Models;

use Phalcon\Mvc\Model;

class Languages extends Model
{
    public $lang_id;
    public $name;
    public $code;
    public $locale;
    public $image;
    public $directory;
    public $sort_order;
    public $status;

    public function initialize()
    {
        $this->setSource("language");
    }

}