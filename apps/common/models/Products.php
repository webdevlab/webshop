<?php

namespace Webshop\Models;

class Products extends \Phalcon\Mvc\Model
{
    public $id;
    public $model;
    public $sku;
    public $upc;
    public $ean;
    public $jan;
    public $isbn;
    public $mpn;
    public $quantity;
    public $stock_status_id;
    public $image;
    public $manufacturer_id;
    public $shipping;
    public $price;
    public $available_at;
    public $subtract;
    public $minimum;
    public $sort_order;
    public $status;
    public $created_at;
    public $modified_at;
    public $viewed;

    public function initialize()
    {
        $this->setSource("product");

        $this->hasMany('id', 'Webshop\Models\ProductsDescription', 'product_id', array(
            'alias' => 'description',
            'reusable' => true
        ));

        $this->hasManyToMany(
            'id',
            'Webshop\Models\CategoriesProducts',
            'product_id',
            'category_id',
            'Webshop\Models\Categories',
            'id',
            array('alias' => 'categories')
        );

    }

    public static function findWithDescription($modelsManager, $langId, $params=null) {
        $builder = $modelsManager->createBuilder();
        $builder->columns('data.*, description.*')
            ->addFrom('Webshop\Models\Products', 'data')
            ->join('Webshop\Models\ProductsDescription', 'data.id = description.product_id', 'description')
            ->where('description.lang_id = :langId:', array('langId' => $langId));
        if($params){
            foreach ($params as $key => $value) {
                $builder->andWhere('data.' . $key . ' = :' . $key . ':', array($key => $value));
            }
        }
        return $builder->getQuery()->execute();
    }

    /**
     * @param mixed $available_at
     */
    public function setAvailableAt($available_at)
    {
        $this->available_at = $available_at;
    }

    /**
     * @return mixed
     */
    public function getAvailableAt()
    {
        return $this->available_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $ean
     */
    public function setEan($ean)
    {
        $this->ean = $ean;
    }

    /**
     * @return mixed
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $jan
     */
    public function setJan($jan)
    {
        $this->jan = $jan;
    }

    /**
     * @return mixed
     */
    public function getJan()
    {
        return $this->jan;
    }

    /**
     * @param mixed $manufacturer_id
     */
    public function setManufacturerId($manufacturer_id)
    {
        $this->manufacturer_id = $manufacturer_id;
    }

    /**
     * @return mixed
     */
    public function getManufacturerId()
    {
        return $this->manufacturer_id;
    }

    /**
     * @param mixed $minimum
     */
    public function setMinimum($minimum)
    {
        $this->minimum = $minimum;
    }

    /**
     * @return mixed
     */
    public function getMinimum()
    {
        return $this->minimum;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $modified_at
     */
    public function setModifiedAt($modified_at)
    {
        $this->modified_at = $modified_at;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * @param mixed $mpn
     */
    public function setMpn($mpn)
    {
        $this->mpn = $mpn;
    }

    /**
     * @return mixed
     */
    public function getMpn()
    {
        return $this->mpn;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return mixed
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $isbn
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }

    /**
     * @return mixed
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @param mixed $sort_order
     */
    public function setSortOrder($sort_order)
    {
        $this->sort_order = $sort_order;
    }

    /**
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->sort_order;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $stock_status_id
     */
    public function setStockStatusId($stock_status_id)
    {
        $this->stock_status_id = $stock_status_id;
    }

    /**
     * @return mixed
     */
    public function getStockStatusId()
    {
        return $this->stock_status_id;
    }

    /**
     * @param mixed $subtract
     */
    public function setSubtract($subtract)
    {
        $this->subtract = $subtract;
    }

    /**
     * @return mixed
     */
    public function getSubtract()
    {
        return $this->subtract;
    }

    /**
     * @param mixed $upc
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;
    }

    /**
     * @return mixed
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * @param mixed $viewed
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
    }

    /**
     * @return mixed
     */
    public function getViewed()
    {
        return $this->viewed;
    }



}